# JsOS 🚀

An linux-based operating system that runs Javascript code at the system-level.

## Documentation

See [the wiki](https://gitlab.com/creepinson/jsos/-/wikis).
