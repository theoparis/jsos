#!/bin/bash
set -eo pipefail

#  -nographic -append "console=ttyS0 debug"

qemu-system-x86_64 --enable-kvm \
    -kernel linux/arch/x86/boot/bzImage -initrd initramfs.cpio.gz \
    -m 1024 -net nic,model=virtio -net user \
    -append "root=/dev/ram rdinit=/bin/node -- --input-type=module /bin/init.mjs" \
